package com.google.appengine.mct;

import java.util.Comparator;

public class SortByRegion implements Comparator {

	public int compare(Object o1, Object o2) {
		Employee u1 = (Employee) o1;
		Employee u2 = (Employee) o2;
		return u1.getRegion().compareTo(u2.getRegion());
	}
}
