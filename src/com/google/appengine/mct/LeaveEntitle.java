package com.google.appengine.mct;

import java.io.Serializable;

public class LeaveEntitle  implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id;
	private String region;
	private String addAnnualLeave;
	private String addMaternityLeave;
	private String addBirthdayLeave;
	private String addWeddingLeave;
	private String addCompassionateLeave;
	private String compensationLeaveExp;
	private String hospitalization;
	
	public String getHospitalization() {
		return hospitalization;
	}
	public void setHospitalization(String hospitalization) {
		this.hospitalization = hospitalization;
	}
	public String getCompensationLeaveExp() {
		return compensationLeaveExp;
	}
	public void setCompensationLeaveExp(String compensationLeaveExp) {
		this.compensationLeaveExp = compensationLeaveExp;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getAddAnnualLeave() {
		return addAnnualLeave;
	}
	public void setAddAnnualLeave(String addAnnualLeave) {
		this.addAnnualLeave = addAnnualLeave;
	}
	public String getAddMaternityLeave() {
		return addMaternityLeave;
	}
	public void setAddMaternityLeave(String addMaternityLeave) {
		this.addMaternityLeave = addMaternityLeave;
	}
	public String getAddBirthdayLeave() {
		return addBirthdayLeave;
	}
	public void setAddBirthdayLeave(String addBirthdayLeave) {
		this.addBirthdayLeave = addBirthdayLeave;
	}
	public String getAddWeddingLeave() {
		return addWeddingLeave;
	}
	public void setAddWeddingLeave(String addWeddingLeave) {
		this.addWeddingLeave = addWeddingLeave;
	}
	public String getAddCompassionateLeave() {
		return addCompassionateLeave;
	}
	public void setAddCompassionateLeave(String addCompassionateLeave) {
		this.addCompassionateLeave = addCompassionateLeave;
	}
	

}
