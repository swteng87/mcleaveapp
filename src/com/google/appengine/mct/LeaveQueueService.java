package com.google.appengine.mct;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.datastore.DataStoreUtil;

public class LeaveQueueService extends DataStoreUtil {
	
	public LeaveQueue findLeaveQueueById(String id){
		LeaveQueue leaveQ = new LeaveQueue();
		Entity entity = findEntity(id); 
		leaveQ.setTime((String)entity.getProperty("time"));
		leaveQ.setEmailAdd((String)entity.getProperty("emailAdd"));
		leaveQ.setNumOfDays((String)entity.getProperty("numOfDays"));
		leaveQ.setStartDate((String)entity.getProperty("startDate"));
		leaveQ.setEndDate((String)entity.getProperty("endDate"));
		leaveQ.setLeaveType((String)entity.getProperty("leaveType"));
		leaveQ.setSupervisor((String)entity.getProperty("supervisor"));
		leaveQ.setRemark((String)entity.getProperty("remark"));
		leaveQ.setProjectName((String)entity.getProperty("projectName"));
		leaveQ.setChangeType((String)entity.getProperty("changeType"));
		leaveQ.setAttachmentUrl((String)entity.getProperty("attachmentUrl"));
		leaveQ.setId(KeyFactory.keyToString(entity.getKey()));
		return leaveQ;
	}
	
	public String addLeaveQueue(String time, String emailAdd, String numOfDays, String startDate, String endDate, 
			String leaveType, String supervisor, String remark, String projectName, String changeType,
			String attachmentUrl) {
		Entity leaveQueueEntity = new Entity(LeaveQueue.class.getSimpleName());
		leaveQueueEntity.setProperty("time", time);
		leaveQueueEntity.setProperty("emailAdd", emailAdd);
		leaveQueueEntity.setProperty("numOfDays", numOfDays);
		leaveQueueEntity.setProperty("startDate", startDate);
		leaveQueueEntity.setProperty("endDate", endDate);
		leaveQueueEntity.setProperty("leaveType", leaveType);
		leaveQueueEntity.setProperty("supervisor", supervisor);
		leaveQueueEntity.setProperty("remark", remark);
		leaveQueueEntity.setProperty("projectName", projectName);
		leaveQueueEntity.setProperty("changeType", changeType);
		leaveQueueEntity.setProperty("attachmentUrl", attachmentUrl);
		getDatastore().put(leaveQueueEntity);
		return KeyFactory.keyToString(leaveQueueEntity.getKey());   
	}
	
	public void updateLeaveQueue(String id, String time, String emailAdd, String numOfDays, String startDate, String endDate, 
			String leaveType, String supervisor, String remark, String projectName, String changeType,
			String approveId, String attachmentUrl) {
		
		Filter leaveQueueFilter = new FilterPredicate("__key__",
                FilterOperator.EQUAL,
                KeyFactory.stringToKey(id));
		Query query = new Query(LeaveQueue.class.getSimpleName()).setFilter(leaveQueueFilter);
		Entity leaveQueueEntity =  getDatastore().prepare(query).asSingleEntity();
		//is existing leave queue record
		if(leaveQueueEntity!=null){
		leaveQueueEntity.setProperty("time", time);
		leaveQueueEntity.setProperty("emailAdd", emailAdd);
		leaveQueueEntity.setProperty("numOfDays", numOfDays);
		leaveQueueEntity.setProperty("startDate", startDate);
		leaveQueueEntity.setProperty("endDate", endDate);
		leaveQueueEntity.setProperty("leaveType", leaveType);
		leaveQueueEntity.setProperty("supervisor", supervisor);
		leaveQueueEntity.setProperty("remark", remark);
		leaveQueueEntity.setProperty("projectName", projectName);
		leaveQueueEntity.setProperty("changeType", changeType);
		leaveQueueEntity.setProperty("attachmentUrl", attachmentUrl);
		getDatastore().put(leaveQueueEntity);  
		}else{
			//is new leave queue record
			Entity entity = new Entity(LeaveQueue.class.getSimpleName());
			entity.setProperty("time", time);
			entity.setProperty("emailAdd", emailAdd);
			entity.setProperty("numOfDays", numOfDays);
			entity.setProperty("startDate", startDate);
			entity.setProperty("endDate", endDate);
			entity.setProperty("leaveType", leaveType);
			entity.setProperty("supervisor", supervisor);
			entity.setProperty("remark", remark);
			entity.setProperty("projectName", projectName);
			entity.setProperty("changeType", changeType);
			entity.setProperty("approveId", approveId);
			entity.setProperty("attachmentUrl", attachmentUrl);
			getDatastore().put(entity);
		}
	}
	
	
	public List<LeaveQueue> getLeaveQueue() {
		Query query = new Query(LeaveQueue.class.getSimpleName());
		List<LeaveQueue> results = new ArrayList<LeaveQueue>();
		for (Entity entity :  getDatastore().prepare(query).asIterable(FetchOptions.Builder.withDefaults())) {
			LeaveQueue leaveQ = new LeaveQueue();
			leaveQ.setTime((String)entity.getProperty("time"));
			leaveQ.setEmailAdd((String)entity.getProperty("emailAdd"));
			leaveQ.setNumOfDays((String)entity.getProperty("numOfDays"));
			leaveQ.setStartDate((String)entity.getProperty("startDate"));
			leaveQ.setEndDate((String)entity.getProperty("endDate"));
			leaveQ.setLeaveType((String)entity.getProperty("leaveType"));
			leaveQ.setSupervisor((String)entity.getProperty("supervisor"));
			leaveQ.setRemark((String)entity.getProperty("remark"));
			leaveQ.setProjectName((String)entity.getProperty("projectName"));
			leaveQ.setChangeType((String)entity.getProperty("changeType"));
			leaveQ.setAttachmentUrl((String)entity.getProperty("attachmentUrl"));
			leaveQ.setApproveId((String)entity.getProperty("approveId"));
			leaveQ.setId(KeyFactory.keyToString(entity.getKey()));
			results.add(leaveQ);
		}
		/* Sort leave queue descendingly based on time */
		Collections.sort(results, new SortLeaveQueue());
		return results;
	}
	
	
	public void deleteLeaveQueue(String id) {
		Transaction txn =  getDatastore().beginTransaction();
		try {
			Filter leaveQueueFilter = new FilterPredicate("__key__",
                    FilterOperator.EQUAL,
                    KeyFactory.stringToKey(id));
			Query query = new Query(LeaveQueue.class.getSimpleName()).setFilter(leaveQueueFilter);
			Entity entity =  getDatastore().prepare(query).asSingleEntity();
			 getDatastore().delete(entity.getKey());
			txn.commit();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
	}
}
