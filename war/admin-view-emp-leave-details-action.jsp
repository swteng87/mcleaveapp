<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>

<%@page import="com.google.appengine.mct.*"%>
<%@page import="com.google.appengine.util.*"%>
<%@page import="java.util.*" %>
<%@page import="java.text.SimpleDateFormat" %>
<%
	String isAdmin = (String)request.getAttribute("isAdmin");
	EmployeeLeaveDetails eld = (EmployeeLeaveDetails)request.getAttribute("eld");
	String yearSelected = (String)request.getAttribute("yearSelected");
	String lastYear = (String)request.getAttribute("lastYear");
	String entitledTotal = (String)request.getAttribute("entitledTotal");
	String others = (String)request.getAttribute("others");
	String currentYear = (String)request.getAttribute("currentYear");
	String nextYear = (String)request.getAttribute("nextYear");
	String allowSickLeave = (String)request.getAttribute("allowSickLeave");
	String lastYearBalance = eld.getLastYearBalance() == null ? "0" : eld.getLastYearBalance();
	String fullName = (String) request.getAttribute("fullName"); if (fullName == null) { fullName = ""; }
	
	List<ApprovedLeave> appList = (ArrayList<ApprovedLeave>)request.getAttribute("appList");
	Double balance = Double.parseDouble(lastYearBalance)+Double.parseDouble(eld.getEntitledAnnual());
	Double totalBalance = Double.parseDouble(lastYearBalance)+Double.parseDouble(eld.getEntitledAnnual());
	SimpleDateFormat standardDF = new SimpleDateFormat(ConstantUtils.DATE_FORMAT);
	Double compLeave = Double.parseDouble(eld.getEntitledCompensation());
	Double sickLeave = Double.parseDouble(allowSickLeave);;
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Master Concept Admin</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
		<link rel="stylesheet" href="css/new-style.css" type="text/css" media="screen" />
		<link rel="shortcut icon" href="images/favicon.ico" sizes="64x64" type="image/png"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet"/>
    <style>
      body { padding-top: 45px; /* 60px to make the container go all the way
      to the bottom of the topbar */
      padding-bottom: 40px;
       }
       .sidebar-nav {
        padding: 0px 0;
      }
      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <link href="assets/css/docs.css" rel="stylesheet"/>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet"/>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
      </script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png"/>
    <style>
      /* 
	Max width before this PARTICULAR table gets nasty
	This query will take effect for any screen smaller than 760px
	and also iPads specifically.
	*/
	@media 
	only screen and (max-width: 760px),
	(min-device-width: 768px) and (max-device-width: 1024px)  {
	
		/* Force table to not be like tables anymore */
		#empDetailList table, 
 		 #empDetailList thead,  
 		 #empDetailList tbody,  
 		 #empDetailList th, 
 		  #empDetailList td,  
 		  #empDetailList tr {  
 			display: block;  
 		} 
		
		/* Hide table headers (but not display: none;, for accessibility) */* 		
		#empDetailList thead tr {  
 			position: absolute; 
 			top: -9999px; 
 			left: -9999px; 
 		} 
		
 		#empDetailList tr { border: 1px solid #ccc; } 
		
 		#empDetailList td {  
/* 			/* Behave  like a "row" */ 
/* /*  		border: none;  */ 
/* /* 			border-bottom: 1px solid #eee;  */ 
			position: relative; 
			padding-left: 50%;  
 			white-space: normal; 
  			text-align:left; 
		}
		
 		#empDetailList td:before {  
/*  			 Now like a table header  */
 			position: absolute; 
/*  			Top/left values mimic padding  */
 			top: 6px; 
 			left: 6px; 
 			width: 45%;  
 			padding-right: 10px;  
 			white-space: nowrap; 
 			text-align:left; 
 		} 
		
		/*
		Label the data
		*/
  		#empDetailList td:nth-of-type(1):before { content: "Particulars"; }  
 		#empDetailList td:nth-of-type(2):before { content: "Entitled"; }  
 		#empDetailList td:nth-of-type(3):before { content: "No Pay"; }  
 		#empDetailList td:nth-of-type(4):before { content: "Sick"; } 
 		#empDetailList td:nth-of-type(5):before { content: "Annual"; }  
  		#empDetailList td:nth-of-type(6):before { content: "Compensate"; }
  		#empDetailList td:nth-of-type(7):before { content: "Others"; }  
  		#empDetailList td:nth-of-type(8):before { content: "Others"; }  
  		#empDetailList td:nth-of-type(9):before { content: "Balance"; }  
 		#empDetailList td:nth-of-type(10):before { content: "Reason for Others"; }   
 		#employeeList td:before { content: attr(data-title); } 
	}
	
	/* Smartphones (portrait and landscape) ----------- */
	@media only screen
	and (min-device-width : 320px)
	and (max-device-width : 480px) {
		body { 
			padding: 0; 
			margin: 0; 
			width: 320px; }
		}
	
	/* iPads (portrait and landscape) ----------- */
	@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
		body { 
			width: 495px; 
		}
	}
    </style>
    <link rel="stylesheet" type="text/css" href="assets/css/datatable-bootstrap.css"/>
    <script src="media/js/jquery.js" type="text/javascript"></script>
    <script src="media/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/datatable-bootstrap.js"></script>
	<script src="media/js/jquery.blockUI.js" type="text/javascript"></script>
	<script src="assets/js/common.js"></script>
		<script language="JavaScript">
		
		</script>
	</head>
	<body>
		<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
      <div class="container">
      <%if(ConstantUtils.TRUE.equals(isAdmin)){ %>
      		<jsp:include page="top-menu.jsp"></jsp:include>
      <%}else{ %>
      		<jsp:include page="top-emp-menu.jsp"></jsp:include>
      <%} %>
      
      </div>
      </div>
    </div>
	<div class="container-fluid">
    <div class="row-fluid">
    <div class="span3" >
    <!-- <div class="well sidebar-nav"> -->
    <%if(ConstantUtils.TRUE.equals(isAdmin)){ %>
    		<jsp:include page="leave-menu.jsp"></jsp:include>
    <%}else{ %>
      		<jsp:include page="leave-emp-menu.jsp"></jsp:include>
    <%} %>
    <!-- </div> -->
    </div>
    <div class="span9">
				<h5 id="Employees Leave Details">EMPLOYEE LEAVE DETAILS</h5>
				<hr></hr>
				<jsp:useBean id="jspbeans" scope="page" class="com.google.appengine.mct.Misc"></jsp:useBean>
				<jsp:setProperty name="jspbeans" property="*"/>
				<jsp:useBean id="jspbeans2" scope="page" class="com.google.appengine.mct.ViewEmpLeaveDetails"></jsp:useBean>
				<jsp:setProperty name="jspbeans2" property="*" />
<!-- 				<table cellpadding="5" border="0" > -->
<%-- 					<tr><td width="160px">Name</td><td><input type="text" value="<%=eld.getName() %>" readOnly/></td></tr> --%>
<%-- 					<tr><td>Year</td><td><input type="text" value="<%=eld.getYear() %>" readOnly/></td></tr> --%>
<%-- 					<tr><td>Last Year's Balance</td><td><input type="text" value="<%=eld.getLastYearBalance() %>" readOnly/></td></tr> --%>
<%-- 					<tr><td>Entitled Annual</td><td><input type="text" value="<%=eld.getEntitledAnnual() %>" readOnly/></td></tr> --%>
<%-- 					<tr><td>Entitled Compensation</td><td><input type="text" value="<%=eld.getEntitledCompensation() %>" readOnly/></td></tr> --%>
<%-- 					<tr><td>Annual Leave</td><td><input type="text" value="<%=eld.getAnnualLeave() %>" readOnly/></td></tr> --%>
<%-- 					<tr><td>Sick Leave</td><td><input type="text" value="<%=eld.getSickLeave() %>" readOnly/></td></tr> --%>
<%-- 					<tr><td>Birthday Leave</td><td><input type="text" value="<%=eld.getBirthdayLeave() %>" readOnly/></td></tr> --%>
<%-- 					<tr><td>No Pay Leave</td><td><input type="text" value="<%=eld.getNoPayLeave() %>" readOnly/></td></tr> --%>
<%-- 					<tr><td>Compensation Leave</td><td><input type="text" value="<%=eld.getCompensationLeave() %>" readOnly/></td></tr> --%>
<%-- 					<tr><td>Compassionate Leave</td><td><input type="text" value="<%=eld.getCompassionateLeave() %>" readOnly/></td></tr> --%>
<%-- 					<tr><td>Maternity Leave</td><td><input type="text" value="<%=eld.getMaternityLeave() %>" readOnly/></td></tr> --%>
<%-- 					<tr><td>Wedding Leave</td><td><input type="text" value="<%=eld.getWeddingLeave() %>" readOnly/></td></tr> --%>
<%-- 					<tr><td>Balance</td><td><input type="text" value="<%=eld.getBalance() %>" readOnly/></td></tr> --%>
<%-- 					<%//=jspbeans2.selectedEmpDetailsSb()%> --%>
<!-- 				</table> -->
				
				<!-- <h5>Transaction Table:</h5> -->
				<div style="float:left; padding-right:26px;"><h5>Transaction Table:</h5></div>
				
					<div>
						<input style="float:center; border:0px solid; box-shadow:none; padding-top:10px;
						font-family: arial; font-size: 14px; font-weight:bold; color:#bd362f;" 
						type="text" name="fullName" value="<%=fullName%>" />
					</div>	
				<table style="table-layout: fixed;" id="empDetailList" cellpadding="0" cellspacing="0" 
				 class="table table-striped table-bordered table-hover" >
					<thead>
					<tr bgcolor="#F0F0F0">
						<th style="width:100px; word-wrap:break-word; align:center;">Particulars</th>
						<th style="width:55px; word-wrap:break-word; align:center;">Entitled</th>
						<th style="width:55px; word-wrap:break-word; align:center;">No Pay</th>
						<th style="width:55px; word-wrap:break-word; align:center;">Sick</th>
						<th style="width:55px; word-wrap:break-word; align:center;">Annual</th>
						<th style="width:70px; word-wrap:break-word; align:center;">Compensate</th>
						<th style="width:55px; word-wrap:break-word; align:center;">Others</th>
						<th style="width:55px; word-wrap:break-word; align:center;">Other Type</th>
						<th style="width:55px; word-wrap:break-word; align:center;">Balance</th>
						<th style="width:180px; word-wrap:break-word; align:center;">Reason for Others</th>
					</tr>
					</thead>
					
					<tbody>
					<tr>
						<td>Balance b/d ending 31 Mar <%=currentYear %></td>
						<td><%=eld.getLastYearBalance() %></td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td><%=eld.getLastYearBalance() %></td>
						<td>-</td>
					</tr>
					<tr>
						<td>From 1 Apr <%=currentYear %>  to 31 Mar <%=nextYear %></td>
						<td><%=eld.getEntitledAnnual() %></td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td><%=balance %></td>
						<td>-</td>
					</tr>
					<%
					if(appList!= null && !appList.isEmpty()){
					for(ApprovedLeave approvedLeave : appList){ 
					%>
					<%if (ConstantUtils.COMPENSATION_LEAVE_ENTITLEMENT.equals(approvedLeave.getLeaveType())) {
							balance = balance + Double.parseDouble(approvedLeave.getNumOfDays());
					%>
					<tr>
						<td><%=approvedLeave.getTime() %></td>
						<td><%=approvedLeave.getNumOfDays() %></td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td><%=balance %></td>
						<td>
							<%=approvedLeave.getRemark() %>
							<br>
							<label style="font-weight:bold; color:#bd362f;">Project Name: </label>
							<%=approvedLeave.getProjectName() %>
						</td>
					</tr>	
					<%} %>
						
					<% if (ConstantUtils.NO_PAY_LEAVE.equals(approvedLeave.getLeaveType())) {
					%>	
					<tr>
						<td><%=approvedLeave.getStartDate() %> - <%=approvedLeave.getEndDate() %></td>
						<td>-</td>
						<td><%=approvedLeave.getNumOfDays() %></td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td><%=balance %></td>
						<td><%=approvedLeave.getRemark() %></td>
					</tr>
					<%} %>
					
					<%if(ConstantUtils.SICK_LEAVE.equals(approvedLeave.getLeaveType())) {%>
					<tr>
						<td><%=approvedLeave.getStartDate() %> - <%=approvedLeave.getEndDate() %></td>
						<td>-</td>
						<td>-</td>
						<td><%=approvedLeave.getNumOfDays() %></td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td><%=balance %></td>
						<td style="width:180px; word-wrap:break-word; align:center;"><%=approvedLeave.getRemark() %> - <a href="<%=approvedLeave.getAttachmentUrl() %>"><%=approvedLeave.getAttachmentUrl() %></a></td>
					</tr>
					<%} %>
					
					<%if(ConstantUtils.ANNUAL_LEAVE.equals(approvedLeave.getLeaveType())) {
						balance = balance - Double.parseDouble(approvedLeave.getNumOfDays());
					%>
					<tr>
						<td><%=approvedLeave.getStartDate() %> - <%=approvedLeave.getEndDate() %></td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td><%=approvedLeave.getNumOfDays() %></td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td><%=balance %></td>
						<td><%=approvedLeave.getRemark() %></td>
					</tr>
					<%} %>
					
					<%if(ConstantUtils.COMPENSATION_LEAVE.equals(approvedLeave.getLeaveType())) { 
						balance = balance - Double.parseDouble(approvedLeave.getNumOfDays());
					%>
					<tr>
						<td><%=approvedLeave.getStartDate() %> - <%=approvedLeave.getEndDate() %></td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td><%=approvedLeave.getNumOfDays() %></td>
						<td>-</td>
						<td>-</td>
						<td><%=balance %></td>
						<td><%=approvedLeave.getRemark() %></td>
					</tr>	
					<%} %>
					
					<%if(ConstantUtils.COMPANSSIONATE_LEAVE.equals(approvedLeave.getLeaveType())) {%>
					<tr>
						<td><%=approvedLeave.getStartDate() %> - <%=approvedLeave.getEndDate() %></td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td><%=approvedLeave.getNumOfDays() %></td>
						<td>-</td>
						<td><%=balance %></td>
						<td><%=ConstantUtils.COMPANSSIONATE_LEAVE %></td>
					</tr>
					<%} %>
					
					<%if(ConstantUtils.BIRTHDAY_LEAVE.equals(approvedLeave.getLeaveType())) {%>
					<tr>
						<td><%=approvedLeave.getStartDate() %> - <%=approvedLeave.getEndDate() %></td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td><%=approvedLeave.getNumOfDays() %></td>
						<td>-</td>
						<td><%=balance %></td>
						<td><%=ConstantUtils.BIRTHDAY_LEAVE %></td>
					</tr>
					<%} %>
					
					<%if(ConstantUtils.MATERNITY_LEAVE.equals(approvedLeave.getLeaveType())) {%>
					<tr>
						<td><%=approvedLeave.getStartDate() %> - <%=approvedLeave.getEndDate() %></td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td><%=approvedLeave.getNumOfDays() %></td>
						<td>-</td>
						<td><%=balance %></td>
						<td><%=ConstantUtils.MATERNITY_LEAVE %></td>
					</tr>
					<%} %>
					
					<%if(ConstantUtils.WEDDING_LEAVE.equals(approvedLeave.getLeaveType())) {%>
					<tr>
						<td><%=approvedLeave.getStartDate() %> - <%=approvedLeave.getEndDate() %></td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td><%=approvedLeave.getNumOfDays() %></td>
						<td>-</td>
						<td><%=balance %></td>
						<td><%=ConstantUtils.WEDDING_LEAVE %></td>
					</tr>
					<%} %>
					
					<% if (ConstantUtils.OTHERS.equals(approvedLeave.getLeaveType())) {
					%>	
					<tr>
						<td><%=approvedLeave.getStartDate() %> - <%=approvedLeave.getEndDate() %></td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td><%=approvedLeave.getNumOfDays() %></td>
						<td><%=balance %></td>
						<td><%=approvedLeave.getRemark() %></td>
					</tr>
					<%} %>
					
					
					<%} %>
					<%}else{ %>
					<tr>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>					
					</tr>
					<%} %>
					<tr>
						<td>Total</td>
						<td><%=entitledTotal %></td>
						<td><%=eld.getNoPayLeave() %></td>
						<td><%=eld.getSickLeave() %></td>
						<td><%=eld.getAnnualLeave() %></td>
						<td><%=eld.getCompensationLeave() %></td>
						<td><%=others %></td>
						<td><%=eld.getOthers() %></td>
						<td><%=Double.parseDouble(entitledTotal) - (Double.parseDouble(eld.getAnnualLeave())+Double.parseDouble(eld.getCompensationLeave())) %></td>
						<td>-</td>
					</tr>
					<tr>
						<td>Balance</td>
						<td><%=entitledTotal %></td>
						<td><%=eld.getNoPayLeave() %></td>
						<td><%=sickLeave - Double.parseDouble(eld.getSickLeave()) %></td>
						<td><%=totalBalance - Double.parseDouble(eld.getAnnualLeave()) %></td>
						<td><%=compLeave - Double.parseDouble(eld.getCompensationLeave()) %></td>
						<td><%=others %></td>
						<td><%=eld.getOthers() %></td>
						<td><%=Double.parseDouble(entitledTotal) - (Double.parseDouble(eld.getAnnualLeave())+Double.parseDouble(eld.getCompensationLeave())) %></td>
						<td>-</td>
					</tr>
					</tbody>
<%-- 					<%//=jspbeans2.transactionDetailsSb()%> --%>
				</table>
			</div>
			</div></div>
	</body>
</html>