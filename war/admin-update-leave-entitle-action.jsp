<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>

<%@page import="com.google.appengine.mct.*"%>
<%@page import="com.google.appengine.util.*"%>
<%@page import="java.util.List"%>
<%
String addAnnualLeave = (String) request.getAttribute("addAnnualLeave"); 
if (addAnnualLeave == null) { addAnnualLeave = ""; }

String addMaternityLeave = (String) request.getAttribute("addMaternityLeave"); 
if (addMaternityLeave == null) { addMaternityLeave = ""; }

String addBirthdayLeave = (String) request.getAttribute("addBirthdayLeave");
if (addBirthdayLeave == null) { addBirthdayLeave = ""; }

String addWeddingLeave = (String) request.getAttribute("addWeddingLeave"); 
if (addWeddingLeave == null) { addWeddingLeave = ""; }

String addCompassionateLeave = (String) request.getAttribute("addCompassionateLeave"); 
if (addCompassionateLeave == null) { addCompassionateLeave = ""; }

String compensationLeaveExp = (String) request.getAttribute("compensationLeaveExp"); 
if (compensationLeaveExp == null) { compensationLeaveExp = ""; }

String region = (String) request.getAttribute("region"); 
if (region == null) { region = ""; }

String leaveEntitleId = (String) request.getAttribute("id"); 
if (leaveEntitleId == null) { leaveEntitleId = ""; }

String hospitalization = (String) request.getAttribute("hospitalization"); 
if (hospitalization == null) { hospitalization = ""; }


List<SickLeave> sickLeaveList = (List<SickLeave>)request.getAttribute("sickLeavelist");

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<title>Master Concept Admin</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
		<link rel="stylesheet" href="css/new-style.css" type="text/css" media="screen" />
		<link rel="shortcut icon" href="images/favicon.ico" sizes="64x64" type="image/png"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet"/>
    <style>
      body { padding-top: 45px; /* 60px to make the container go all the way
      to the bottom of the topbar */
      padding-bottom: 40px;
       }
       .sidebar-nav {
        padding: 0px 0;
      }
      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
     <link href="assets/css/docs.css" rel="stylesheet"/>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet"/>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
      </script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png"/>
    <style>
      undefined
    </style>
    <link rel="stylesheet" type="text/css" href="assets/css/datatable-bootstrap.css"/>
    <script src="media/js/jquery.js" type="text/javascript"></script>
    <script src="media/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/datatable-bootstrap.js"></script>
	<script src="media/js/jquery.blockUI.js" type="text/javascript"></script>
	<script src="assets/js/common.js"></script>
		<script language="JavaScript">
		function Go() {
				if(allMandatory() == true){
					$('#myModal').modal('show');
					return false;
				}
			    return true;
		}
    	
    	function allMandatory() {
			var bool = false;
    		
    		$('input, select').each(function() {
                 if(this.value == "" || this.value == "undefined"){
                 	bool = true;
                 }
            });
    		
    		return bool;
	 	}
    	
  			function clearForm() {
  				var f_elements = document.forms[0].elements;
  				for (i = 0; i < f_elements.length; i++) {
    				field_type = f_elements[i].type.toLowerCase();
    				switch (field_type) {
    					case "text":
    					case "password":
						case "textarea":
    					case "hidden":
    						f_elements[i].value = "";
        					break;
    					case "radio":
    					case "checkbox":
        					if (f_elements[i].checked) {
            					f_elements[i].checked = false;
        					}
        					break;
    					case "select-one":
    						f_elements[i].selectedIndex = 0;
    						break;
    					case "select-multi":
        					f_elements[i].selectedIndex = -1;
        					break;
    					default:
        					break;
    				}
				}
  			}
		</script>
		<script type="text/javascript">
        $(document).ready(function () {
        			       
        	$("#update-leaveEntitle").on("click",function(){      		
        		
            	if(Go()){
            		$(".span9").block({
    	    		 	showOverlay: true, 
    	    	        centerY: true, 
    	    	        css: { 
    	    	            width: '200px',  
    	    	            border: 'none', 
    	    	            padding: '15px', 
    	    	            backgroundColor: '#000', 
    	    	            '-webkit-border-radius': '10px', 
    	    	            '-moz-border-radius': '10px', 
    	    	            opacity: .6, 
    	    	            color: '#fff' 
    	    	        }, 
    	    			message: '<font face="arial" size="4">Loading ...</font>'
    	    			
    	    		});
            		
            		var data = $("form").serialize();
            		
            		
    	    		$.ajax({
    	    			type: "POST",
    	    			url: "/UpdateLeaveEntitleAction",
    	    			data: data,
    	    			success: function(response) {
    	    				$(".span9").unblock();
    	    				
    	    				$("#msg").html(response);
    	                	
    	    			}
    	    		});
            	}
            	
    				
            	});
        	
        	
        	$(".remove").on("click",function(){
        		
        		$('#myModalWarning').modal('show');
        		
        		var id = $(this).closest("span").attr("id");
				
				$("#remove-sl").click(function(){
					
					$.get("/UpdateLeaveEntitleAction",{id:id},function(response) {
						$("span").each(function(){
							if($(this).attr("id") == id){
								$(this).remove();
							}
						});
    	    		});
					
					
	        		
	        	});
        		
        			
			});
        	
        	$("#add-sick-leave").on("click",function(){
       		 $.get("sick-leave.jsp", function (response) {
       			 $("#sick-leave").append(response);
                });
       		
       		});
        	
        	
        });
		</script>
	</head>
	<body>
		<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
      <div class="container">
      <jsp:include page="top-menu.jsp"></jsp:include>
      </div>
      </div>
    </div>
	<div class="container-fluid">
    <div class="row-fluid">
    <div class="span3" >
<!--     <div class="well sidebar-nav"> -->
    <jsp:include page="configuration-menu.jsp"></jsp:include>
<!--     </div> -->
    </div>
    <div class="span9">
				<h5 id="Leave Entitle">UPDATE LEAVE ENTITLE</h5>
				<hr></hr>
				<div id="msg" class="error-msg"></div>
				<form name="Update" method="post" action="UpdateLeaveEntitleAction">
					<input type="hidden" name="id" value="<%=leaveEntitleId%>"/>
					<jsp:useBean id="jspbeans" scope="page" class="com.google.appengine.mct.Misc"></jsp:useBean>
					<jsp:setProperty name="jspbeans" property="*" />
					<table cellpadding="10" style="color:black;" border="0">
						<%//=jspbeans.listSettings()%>
						<tr>
							<td>Region</td>
							<td><input type="text" name="region" value="<%=region %>" maxlength="10" readonly/></td>
						</tr>
						<tr>
							<td>Entitled Annual Leave</td>
							<td><input type="text" name="addAnnualLeave" value="<%=addAnnualLeave %>" maxlength="4"/></td>
						</tr>
						<tr>
							<td>Maternity Leave</td>
							<td><input type="text" name="addMaternityLeave" value="<%=addMaternityLeave %>" maxlength="4"/></td>
						</tr>
						<tr>
							<td>Birthday Leave</td>
							<td><input type="text" name="addBirthdayLeave" value="<%=addBirthdayLeave %>" maxlength="4"/></td>
						</tr>
						<tr>
							<td>Wedding Leave</td>
							<td><input type="text" name="addWeddingLeave" value="<%=addWeddingLeave %>" maxlength="4"/></td>
						</tr>
						<tr>
							<td>Compassionate Leave</td>
							<td><input type="text" name="addCompassionateLeave" value="<%=addCompassionateLeave %>" maxlength="4"/></td>
						</tr>
						<tr>
							<td>Compensation Entitlement Leave Expire</td>
							<td><input type="text" name="compensationLeaveExp" value="<%=compensationLeaveExp %>" maxlength="4"/></td>
						</tr>
						<tr>
							<td>Hospitalization</td>
							<td><input type="text" name="hospitalization" value="<%=hospitalization %>" maxlength="4"/></td>
						</tr>
						<tr>
							<td colspan="10">
								<fieldset>
								<legend style="font-size:14px;"><b>Sick Leave</b></legend>
								<table width="450px">
									
									<tr>
										<td colspan="2">
										<%for(SickLeave sl : sickLeaveList){ %>
											<span id="<%=sl.getId()%>">
												<input type="hidden" name="sickLeaveId[]" value="<%=sl.getId()%>"/>
												Sick Leave Entitle
												<input type="text" name="sickLeaveDay[]" value="<%=sl.getSickLeaveDay() %>" placeholder="day" class="input-mini" maxlength="4"/>
												
												<select name="sickLeaveType[]" style="width: 160px">
													<option value="<%=ConstantUtils.LESS_THAN %>" <%if(sl.getSickLeaveType().equals(ConstantUtils.LESS_THAN)){ %>selected <%} %>>less than</option>
													<option value="<%=ConstantUtils.LESS_THAN_OR_EQUAL %>" <%if(sl.getSickLeaveType().equals(ConstantUtils.LESS_THAN_OR_EQUAL)){ %>selected <%} %>>less than or equal</option>
													<option value="<%=ConstantUtils.GREATER_THAN %>" <%if(sl.getSickLeaveType().equals(ConstantUtils.GREATER_THAN)){ %>selected <%} %>>greater than</option>
													<option value="<%=ConstantUtils.GREATER_THAN_OR_EQUAL %>" <%if(sl.getSickLeaveType().equals(ConstantUtils.GREATER_THAN_OR_EQUAL)){ %>selected <%} %>>greater than or equal</option>
												</select>
												<input type="text" class="input-mini" name="sickLeaveYear[]" placeholder="year" value="<%=sl.getSickLeaveYear() %>" maxlength="4"/>
												
												<a href="#" class="btn btn-mini remove"><i class="icon-remove"></i></a>
											</span>
						
										<%} %>
											
											<div id="sick-leave"></div>
											<div><a href="#" id="add-sick-leave" class="btn btn-small btn-inverse">Add More</a></div>
										</td>
									</tr>
									
								</table>
								</fieldset>
							</td>
						</tr>
						
						<tr>
							<td></td>
							<td colspan="2" align="right">
								<a href="#" class="btn btn-primary" id="update-leaveEntitle">Update</a>
								&nbsp;&nbsp;&nbsp;
								<a href="#" class="btn btn-danger" onClick="javascript:clearForm()">Reset</a>
								&nbsp;&nbsp;&nbsp;
<!-- 								<a href="admin-new-emp.jsp"><img src="images/End_Button.png" style="border:none;" -->
<!-- 								onmouseover="this.src='images/End_Hover.png'" onmouseout="this.src='images/End_Button.png'"/></a> -->
							</td>
						</tr>
					</table>
				</form>
				
			</div>
			</div></div>
			
			<!-- alert -->
<!-- 				<div class="alert alert-block alert-error hide fade in"> -->
<!--             	<button type="button" class="close">&times;</button>   -->
<!--   				<strong>Warning!</strong> This record will permanent remove.  -->
<!--   				<p></p> -->
<!--                	<a class="btn btn-danger" href="#">Ok</a>  -->
<!--                 <a class="btn" href="#">Cancel</a>  -->
<!-- 				</div>  -->
			<div id="myModalWarning" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  
				  <div class="modal-body">
				  <div class="alert alert-block alert-error"> 
  					<b>Warning!</b> This record will permanent remove.
  					</div>
				  </div>
				  <div class="modal-footer">		
				  	<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" id="remove-sl">Remove</button>
				    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				  </div>
				</div>
			
			
			<!-- Modal -->
				<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
				    <h4 id="myModalLabel">Update Leave Entitle</h4>
				  </div>
				  <div class="modal-body">
				    <p>Please make sure all fields are filled.</p>
				  </div>
				  <div class="modal-footer">			    
				    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Ok</button>
				  </div>
				</div>
				
			
	</body>
</html>