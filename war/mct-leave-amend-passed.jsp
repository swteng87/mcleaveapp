<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>

<%@page import="com.google.appengine.mct.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Master Concept</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script language="javaScript" type="text/javascript" src="script/calendar.js"></script>
   		<link href="css/calendar.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<link rel="shortcut icon" href="images/favicon.ico" sizes="64x64" type="image/png"/>
		<script language="JavaScript">
			function toggle(id) {
    			var n = document.getElementById(id);
    			n.style.display =  (n.style.display != 'none' ? 'none' : '' );
  			}
		</script>
		<!--[if !IE 7]>
		<style type="text/css">
			#wrap {display:table;height:100%}
		</style>
		<![endif]-->
	</head>
	<body>
		<div class="wrapper">
	        <div id="head"></div>
			<div id="content">
				<h4>AMEND / CANCEL LEAVE APPLICATION PASSED</h4>
				<br/>
				<table cellpadding="5" style="color:black;">
					<tr>
						<td style="padding-left:0px;"><img src="images/thankyou.png" width="70px"/></td>
						<td>You have successfully submitted your leave request. Your supervisor will be notified immediately.</td>
					</tr>
				</table>
			</div>
			<div id="menu">
				<ul>
					<li><a href="mct-emp-policy.jsp">Master Concept Leave Policy</a></li>
					<li><a href="ApplyLeave">Master Concept Leave Application Form</a></li>
					<li><a href="ApplyCompLeave">Compensation Leave Entitlement Form</a></li>
					<li style="background:url('images/Arrow_Orange.png');"><a href="mct-leave-amend.jsp">Amend / Cancel Leave</a></li>
					<li><a href="mct-view-history.jsp">View My Leave History</a></li>
					<li><a href="mct-view-emp-leave-details.jsp">View My Leave Details</a></li>
					<li><a href="mct-request-action.jsp">Approve / Reject Requests</a></li>
					<li><a href="mct-view-approved.jsp">View Approved Requests</a></li>
					<li><a href="mct-view-rejected.jsp">View Rejected Requests</a></li>
					<li><a href="log-out.jsp">Log Out</a></li>
				</ul>                                                   
			</div>
	        <div class="push"></div>
	    </div>
	    <div class="footer"></div>
	</body>
<html>