		<ul class="nav nav-list bs-docs-sidenav">
          <li class="side-menu"><a href="UpdateCompanyPolicy">Leave Requests</a></li>
          <li class="side-menu"><a href="mct-request-action.jsp"><i class="icon-play pull-right"></i>Approve / Reject Leave Requests</a></li>
          <li class="side-menu"><a href="ApplyLeave"><i class="icon-play pull-right"></i>Leave Application Form</a></li>
          <li class="side-menu"><a href="ApplyCompLeave"><i class="icon-play pull-right"></i>Compensation Leave Entitlement form</a></li>
          <li class="side-menu"><a href="mct-leave-amend.jsp"><i class="icon-play pull-right"></i>Amend / Cancel Leave</a></li>
          <li class="side-menu"><a href="mct-view-history.jsp"><i class="icon-play pull-right"></i>Leave History</a></li>
          <li class="side-menu"><a href="mct-view-emp-leave-details.jsp"><i class="icon-play pull-right"></i>Leave Details</a></li>               
          <li class="side-menu"><a href="log-out.jsp">Logout</a></li>
        </ul>