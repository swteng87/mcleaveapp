<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Master Concept Admin</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
		<link rel="stylesheet" href="css/new-style.css" type="text/css" media="screen" />
		<link rel="shortcut icon" href="images/favicon.ico" sizes="64x64" type="image/png"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet"/>
    <style>
      body { padding-top: 45px; /* 60px to make the container go all the way
      to the bottom of the topbar */
      padding-bottom: 40px;
       }
       .sidebar-nav {
        padding: 0px 0;
      }
      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet"/>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
      </script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png"/>
    
    <script src="media/js/jquery.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="media/js/jquery.blockUI.js" type="text/javascript"></script>
	<script src="assets/js/common.js"></script>
	</head>
	<body>
		<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
      <div class="container">
      <jsp:include page="top-menu.jsp"></jsp:include>
      </div>
      </div>
    </div>
	<div class="container-fluid">
    <div class="row-fluid">
    <div class="span3" >
    <div class="well sidebar-nav">
    <jsp:include page="configuration-menu.jsp"></jsp:include>
    </div>
    </div>
    <div class="span9">
				<h5 id="Remove data">Remove Data</h5>
				<hr></hr>
				<form method="post" action="RemoveData" name="RemoveData">
					<table class="table table-striped table-bordered" >
						<tr>
							<td>History Table</td><td><input type="checkbox" name="history" value="history"/></td>
						</tr>
						<tr>
							<td>Region Table</td><td><input type="checkbox" name="region" value="region"/></td>
						</tr>
						<tr>
							<td>Regional Holidays Table</td><td><input type="checkbox" name="holidays" value="holidays"/></td>
						</tr>
						<tr>
							<td>Supervisor Table</td><td><input type="checkbox" name="supervisor" value="supervisor"/></td>
						</tr>
					</table>
					<input name="submit" type="submit" value="Remove" class="btn btn-primary"/>
				</form>
				</div>
				</div>
				</div>
				</body>	
	
</html>